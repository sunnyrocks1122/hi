<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <link rel="icon" href="https://scontent-mad1-1.xx.fbcdn.net/v/t1.0-9/15740748_1305980492809291_3295447875611393552_n.jpg?oh=dbabee649a2993434aeed048eba244ed&oe=59236F1F">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Shashank Shekhar</title>
    <link href="css/bootstrap.css" rel="stylesheet">
    <link href="css/font-awesome.min.css" rel="stylesheet">
    <style>
        nav>.active>a, .navbar-default .navbar-nav>.active>a:focus, 
        .navbar-default .navbar-nav>.active>a:hover {
            color: #000;
            background-color :#c4e3f3;
        }
    </style>
  </head>
  <body style="background-color: silver">
      <div class="container well" style="background-color: white; padding-top: 10px;">  
        <nav class="navbar navbar-default navbar-static-top" role="navigation">
                <div class="navbar-header page-scroll">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand page-scroll" href="#page-top">My Profile</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <!-- Hidden li included to remove active class from about link when scrolled up past about section -->
                        <li class="hidden">
                            <a class="page-scroll" href="#intro"></a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#intro">Introduction</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#academic">Academic</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#skills">Skills</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#current">Learning</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#personal">Personal</a>
                        </li>
                        <li>
                            <a class="page-scroll" href="#download">Download CV</a>
                        </li>
                    </ul>
                </div>
                <!-- /.navbar-collapse -->
            <!-- /.container -->
        </nav>
        <section id="intro" class="intro-section">
            <div class="row">
                <div class="col-lg-9">
                  <h1>Shashank Shekhar</h1>
                  <p class="text-justify" style="font-size: 18px;">
                      <i class="fa fa-phone"></i> +91-9956820045<br>
                      <i class="fa fa-envelope-open-o"></i> <a href="mailto:sunnyrocks1122@gmail.com">sunnyrocks1122@gmail.com</a><br>
                      <i class="fa fa-envelope-open-o"></i> <a href="mailto:shashank.shekhar2k15@outlook.com">shashank.shekhar2k15@outlook.com</a><br><br>
                      <a class="col-lg-3" href="https://www.facebook.com/sunnyrocks1122" target="_blank"><i class="fa fa-facebook-official fa-2x"></i></a>
                      <a class="col-lg-3" href="https://twitter.com/sunnyrocks1122" target="_blank"><i class="fa fa-twitter fa-2x"></i></a>
                      <a class="col-lg-3" href="https://www.linkedin.com/in/shashankshekhar2k15" target="_blank"><i class="fa fa-linkedin fa-2x"></i></a>
                      <a class="col-lg-3" href="https://www.instagram.com/shashank.shekhar2k16/" target="_blank"><i class="fa fa-instagram fa-2x"></i></a>
                  </p>
                </div>
                <div class="col-lg-3">
                  <img src="./pic.jpg" class="img img-thumbnail" style="float:right">
                </div>
            </div>
        </section>
        <hr>
        <section id="academic">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Academic Details</h3>
                </div>
                <div class="col-lg-12">
                    <table class="table table-hover table-bordered">
                        <tr class="bg-info"><th colspan="2">Masters of Computer Application</th></tr>
                        <tr><td class="col-lg-2">University</td><td class="col-lg-10">Babasahab Bhimrao Ambedkar University</td></tr>
                        <tr><td class="col-lg-2">Location</td><td class="col-lg-10">Lucknow</td></tr>
                        <tr><td class="col-lg-2">Session</td><td class="col-lg-10">2014-2017</td></tr>
                        <tr><td class="col-lg-2">Aggrigate</td><td class="col-lg-10">7.3 </td></tr>
                        <tr class="bg-info"><th colspan="2">Bachelor of Science - IT</th></tr>
                        <tr><td class="col-lg-2">University</td><td class="col-lg-10">Amity University</td></tr>
                        <tr><td class="col-lg-2">Location</td><td class="col-lg-10">Lucknow</td></tr>
                        <tr><td class="col-lg-2">Session</td><td class="col-lg-10">2011-2014</td></tr>
                        <tr><td class="col-lg-2">Aggrigate</td><td class="col-lg-10">9.3 </td></tr>
                    </table>
                </div>
            </div>
        </section>
        <hr>
        <section id="skills">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Skills</h3>
                </div>
                <div class="col-lg-12 well">
                        <div class="btn btn-lg btn-primary">PHP</div>
                        <div class="btn btn-lg btn-success">HTML</div>
                        <div class="btn btn-lg btn-danger">CSS</div>
                        <div class="btn btn-lg btn-primary">C</div>
                        <div class="btn btn-lg btn-default" style="background-color:#8899aa">JAVA</div>
                        <div class="btn btn-lg btn-warning">Javascript</div>
                        <div class="btn btn-lg btn-danger">JQuery</div>
                        <div class="btn btn-lg btn-primary" style="background-color: #6f5499">Bootstrap</div>
                        <hr>
                        <div class="btn btn-lg btn-warning">MYSQL</div>
                        <div class="btn btn-lg btn-primary">MS-SQL</div>
                        <hr>
                        <div class="btn btn-lg btn-warning" style="background-color: #eeba37">Basic AWS configuration for PHP hosting </div>
                        <div class="btn btn-lg btn-success">Windows server 2012 R2 setting up Active directory</div>
                        <div class="btn btn-lg btn-info" style="background-color: #e95420">UBUNTU</div>
                        <div class="btn btn-lg btn-default" style="background-color: #c0c0c0">GitHub</div>
                        <hr>
                        <div class="btn btn-lg btn-info">M.S.Office</div>
                        <div class="btn btn-lg btn-danger">Dreamweaver</div>
                        <div class="btn btn-lg btn-success">Atom</div>
                        <div class="btn btn-lg btn-default" style="background-color: #a6e1ec">NetBeans</div>
                        <div class="btn btn-lg btn-primary" style="background-color: #3d0099">Visual Studio</div>
                        <div class="btn btn-lg btn-primary">Movie Maker</div>
                </div>
            </div>
        </section>    
        <hr>
        <section id="current">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Currently learning</h3>
                </div>
                <div class="col-lg-12 well">
                        <div class="btn btn-lg btn-primary">PHP 7</div>
                        <div class="btn btn-lg btn-danger" style="background-color: #f4645f">LARAVEL</div>
                        <div class="btn btn-lg btn-warning">CodeIgniter</div>
                        <hr>
                        <div class="btn btn-lg btn-success">MYSQL</div>
                        <hr>
                        <div class="btn btn-lg btn-info" style="background-color: #e95420">Ubuntu Server management and administration </div>
                        <div class="btn btn-lg btn-default" style="background-color: #c0c0c0">GitHub</div>
                        <div class="btn btn-lg btn-danger">Red Hat Cloud</div>
                        <div class="btn btn-lg btn-primary">WordPress</div>
                </div>
            </div>
        </section>    
        <hr>
        <section id="personal">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Personal Details</h3>
                </div>
                <div class="col-lg-12">
                    <table class="table table-hover table-bordered">
                        <tr><td class="col-lg-2">Date of Birth</td><td class="col-lg-10">29 September 1993</td></tr>
                        <tr><td class="col-lg-2">Father's Name</td><td class="col-lg-10">Mr. Satti Deen</td></tr>
                        <tr><td class="col-lg-2">Address</td><td class="col-lg-10">H. No. 18 Satya Puram Colony Balaganj Lucknow, Uttar Pradesh, India.</td></tr>
                        <tr><td class="col-lg-2">Hometown</td><td class="col-lg-10">Lucknow</td></tr>
                    </table>
                </div>
            </div>
        </section>    
        <hr>
        <section id="download">
            <div class="row">
                <div class="col-lg-12">
                    <h3>Download CV</h3>
                </div>
                <div class="col-lg-12">
                    <a href="cv-shashank.pdf" target="_blank"><button class="btn btn-success"><i class="fa fa-download fa-2x"> Download</i></button></a>
                </div>
            </div>
        </section>    
        <hr>
    </div>
      <section>
          <div class="col-lg-12 well"><center>Designed and developed by Shashank Shekhar with the help of Red Hat Cloud and GitHub.</center></div>
      </section>  
      <a href="#intro"><button class="btn btn-success" id="up" style="position: fixed;bottom: 10%; right: 5%;"><i class="fa fa-toggle-up fa-2x"></i></button></a>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script>
        $('#up').on('click', function(e){
            e.preventDefault();
            var target= $('nav');
            $('html, body').stop().animate({
               scrollTop: target.offset().top
            }, 1000);
        });
        function scrollNav() {
        $('.nav a').click(function(){  
          //Toggle Class
          $(".active").removeClass("active");      
          $(this).closest('li').addClass("active");
          var theClass = $(this).attr("class");
          //Animate
          $('html, body').stop().animate({
              scrollTop: $( $(this).attr('href') ).offset().top - 160
          }, 1000);
          return false;
        });
        $('.scrollTop a').scrollTop();      }
      scrollNav();
    </script>
  </body>
</html>
